# Binary ==> Base 10
Author: Matthew Hilton

People that assisted: Jessy Armstrong, Adam Ames, Daniel Wright


How to run it:

In the directory that the bToB.jar file is in, type the following in your command line:

	"java -jar bToB.jar"


Then type in the desired integer and hit enter (Note: There is a 16 bit limit so integers greater than 65535 and less than -65536 will be invalidated)


The result will print to the console.



Known Bugs:

<Resolved> Currently does not take negative integers.



Upcoming Features:

<Added!> Ability to run until done.



Changelog:

1-11-2017
-First commit. Built main and made sure that I knew what I was doing.

2-11-2017
-Got some stuff. Tested out the math and currently working on the possibility of using recursion. However might cut that out for the "Shove then Stuff" method.

2-11-2017
-Built the core part of the code. Uses the aforementioned Shove then Stuff method for the the array. Next need to figure out how to account for negative ints and. Also need to abstract most of the code.

7-11-2017
-Fixed negative integers. Range is -32768 to 32787.
-Started to replace some of the main method with methods. Still needs work.

8-11-2017
-Is now in a while loop so it repeats forever unless exit condition is met.
-UI improvements
-More Comments

1-3-2018
-Made a makefile to compress the files into a jarfile for easy sharing and execution 
-Updated README file

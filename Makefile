JAVASRC 			= bToB.java
SOURCES				= README.md Makefile $(JAVASRC)
MAINCLASS 		= bToB
CLASSES 			= bToB.class
JARFILE				= bToB.jar

all: $(JARFILE)

$(JARFILE): $(CLASSES)
		echo Main-class: $(MAINCLASS) > Manifest
		jar cvfm $(JARFILE) Manifest $(CLASSES)
		rm Manifest

$(CLASSES): $(JAVASRC)
		javac -Xlint $(JAVASRC)

clean:
		rm $(CLASSES) $(JARFILE)

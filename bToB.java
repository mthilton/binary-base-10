import java.util.*;

class bToB {

  //Class Variables
  static int[] BinaryArray;
  static boolean negative;
  static int n;
  static int input;


  //Main method
  static public void main(String[] agrs) {

    //Allows for commandline input
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter an integer to convert from decimal or 'x' to quit:");

    //Checks to see if the exit condition has been met
    while(scan.hasNext()) {
      String toCheck = scan.next();
      if(toCheck.toLowerCase().equals("x")) {
        System.exit(0);
      } else {

        //Grabs user input and checks to see if the Decimal number will convert to fit in 2s comp 16 bi
        input = Integer.parseInt(toCheck);

        if(input<-32768 || input>32767){
          System.out.println("Input Error: Interger not in Bounds. Pick a number that fits in 16 bits\n\n");
          System.out.println("Enter an integer to convert from decimal or 'x' to quit:");
        } else {

          //Initalizing our variables
          n = 15;
          negative = false;
          BinaryArray = new int[16];
          int rawInput = input;

          //Checks to see if input is negative. If negative, sets negative flag and makes Decimal number positive
          if(input<0) {
            negative = true;
            input*=-1;
          }

          //Brute force method for converting from a higher base to base 2. Stores in array
          while(input>0) {
            BinaryArray[n] = input%2;
            input = input/2;
            n--;
          }
          n = 15;

          //Checks to see if negative flag is true. If true, take the 2s Comp
          if(negative == true) {
            System.out.print("Here is the |"+rawInput+"| in unsigned binary: ");
            printArray();
            invertArray();
            isNegative();
            }

          System.out.print("Here it is in 2's Complement Binary: ");
          printArray();
          System.out.println("\n\nEnter an integer to convert from decimal or 'x' to quit:");
          }
        }
      }
      scan.close();
    }

  //Mathod that takes the 1s Comp of the Array
  static void invertArray(){
    while(n>-1){
      if(BinaryArray[n] == 0){
        BinaryArray[n] = 1;
        n--;
        if(n == -1){
          break;
        }
      }
      if(BinaryArray[n] == 1){
        BinaryArray[n] = 0;
        n--;
        if(n == -1){
          break;
        }
      }
    }
    n = 15;
  }

  //Method that adds 1 bitwise
  static void isNegative(){
    BinaryArray[n]++;
    while(BinaryArray[n]>1){
      BinaryArray[n] = BinaryArray[n] - 2;
      n--;
      if(n == -1){
        break;
      }
      BinaryArray[n]++;
    }
  }

  //Method for Printing the Array
  static void printArray(){
    for(int y=0;y<BinaryArray.length;y++) {
    System.out.print(BinaryArray[y]);
    }
    System.out.print("\n");
  }

}
